const url = 'https://my-json-server.typicode.com/kauly/MockJson/db';

export const getData = () => {
    return fetch(url)
        .then(response => response.json())
        .then(jsonResponse => {
            return jsonResponse;
        });
}