import React from 'react';
import './NewsList.css';
import News from '../News/News';

const NewsList = ({ newsVet }) => {
    let mid = 0;
  
    if(!newsVet){
        return <div>Loading..</div>;
        
    }else{
        mid = newsVet.length/2;
        newsVet.splice(mid,0,'');
    }
    
    return (
        <div className="container">
            {
                newsVet.map((news,n) => {
                    if(n === mid){
                        return <span className="middle" key={mid}></span>;
                    } else {
                        return <News key={n} news={news}/>
                    }
                })
            }
            
        </div>
    );
};

export default NewsList;