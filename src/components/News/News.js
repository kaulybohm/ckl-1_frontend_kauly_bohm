import React from 'react';
import './News.css';

const News = ({ news }) => {

    const setSubjectColor = sub => {
        let color = '';
        switch(sub){
            case 'politics':
                color = '#FF001F';
                break;
            case 'science':
                color = '#7CBB37';
                break;
            case 'sports':
                color = '#F5A623'  
                break;
            case 'tech':
                color = '#4A90E2';                          
        }
        return color;   
    }

    let subjectStyle = {
        color: setSubjectColor(news.subject)
    };
   
    
    return (
        
        <div className={news.type}>
            <p className="subject" style={subjectStyle}>{news.subject}</p>
            {
                news.img_news ?  <img className="img-news" 
                                    src={news.img_news} 
                                    height= {news.type === 'main-news'  ?  '354px' : '167px'} 
                                    width = {news.type === 'main-news'  ? '555px' : '262px'} 
                                    alt="News"/>: null
            }
            <span className={news.type === 'main-news'  ?  'main-title' : 'title'}>{news.title}</span>
            <div className="author">            
                    <img className="author-img" src={news.img_author} height="45px" width="45px" alt="Author"/>
                    <span className="author-name">{news.author}</span>             
            </div>
            <span className="resume">
                {news.content}
            </span>
        </div>
    );
};

export default News;