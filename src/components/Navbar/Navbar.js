import React from 'react';
import './Navbar.css';

const Navbar = () => {
    return (
        <header>
            <nav>
                <div className="logo">
                <a href="#"><img src="logo.png" alt="Logo"/></a>
                </div>
                <label htmlFor="toggle">&#9776;</label>
                <input type="checkbox" id="toggle" />
                <ul>
                    <li>
                        <a href="#">politics</a>
                    </li>
                    <li>
                        <a href="#">business</a>
                    </li>
                    <li>
                        <a href="#">tech</a>
                    </li>
                    <li>
                        <a href="#">science</a>
                    </li>
                    <li>
                        <a href="#">sports</a>
                    </li>
                </ul>
            </nav>
        </header>
    );
}

export default Navbar;