import React, { Component } from 'react';
import './App.css';
import Navbar from '../Navbar/Navbar';
import NewsList from '../NewsList/NewsList';
import {getData} from '../../util/newsApi';

class App extends Component{

  constructor(props){
    super(props);
    this.state = {
      newsVet: []
    }
    this.getNews = this.getNews.bind(this);
    this.getNews();
  }

  getNews(){
    getData().then(response => {
      this.setState({newsVet: response.news});
    })
  }

  render(){

    return (
      <div>
        <Navbar />
        <NewsList newsVet={this.state.newsVet} />
      </div>
    );
  }
}

export default App;
