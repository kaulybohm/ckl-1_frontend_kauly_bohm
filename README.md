## Cupcaker Challenge

This is a project build for the cupcaker guided challenge. 

## Installation

Just clone this repositorie and run:

`npm install`

`npm start`

## Backend

I use the excelent [My JSON Server](https://my-json-server.typicode.com/) to create a fake REST API and my fake database can be found [here](https://github.com/kauly/MockJson). The images are hosted in my Google Drive.

## Deploy

To deploy the app, I use [Surge](https://surge.sh/).
The app can be found at:

http://kauly-cupcaker.surge.sh/


